package Exercise2;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.Scanner;

/**
 * Created by maryb on 25.05.2017.
 */
public class BinarySystem {
    public static void main(String[] args) {

        String line;
        int number;

        while(true) {
            Scanner in = new Scanner(System.in);
            System.out.println("Wpisz liczbę: ");
            line = in.nextLine();

            if (line.matches("\\d*")) {
                number = Integer.valueOf(line);

                if(number > 15) {
                    System.out.println("za duża liczba");
                } else {
                    break;
                }
            } else {
                System.out.println("Niepoprawne dane. Podaj liczbę od 0 do 15.");
            }
        }

        int[] numberArray = new int[4];

        for (int i = 3; i>= 0; i--) {
            numberArray[i] = number % 2;
            number /= 2;
        }

        for (int digit : numberArray) {
            System.out.print(digit + " ");
        }
    }
}
