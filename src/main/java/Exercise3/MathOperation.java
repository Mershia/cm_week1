package Exercise3;

/**
 * Created by maryb on 25.05.2017.
 */
public class MathOperation {
    public static void main(String[] args) {


        int[] numberArray = {2, 3, 2, 4, 6, 7, 8, 1, 2, 9};

        System.out.println(numberArray.length);

        // ======*======*=====* SUM
        System.out.println("Dodawanie:");

        int sum = 0;
        for (int i = 0; i < numberArray.length; i++) {
            sum = sum + numberArray[i];
        }
        System.out.println(sum);


        sum = 0;
        for (int i : numberArray) {
            sum += i;
        }
        System.out.println(sum);


        // ======*======*=====* MULTI
        System.out.println("Mnożenie:");

        int multi = 1;

        for (int i = 0; i < numberArray.length; i++) {
            multi *= numberArray[i];
        }
        System.out.println(multi);
    }
}
